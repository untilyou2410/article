<%-- 
    Document   : home
    Created on : Feb 19, 2020, 1:09:45 PM
    Author     : anhnb
--%>

<%@page import="util.HtmlHelper"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="hardCode/css/rightCss.css" rel="stylesheet" type="text/css"/>
<link href="hardCode/css/cssHome.css" rel="stylesheet" type="text/css"/>
<link href="hardCode/css/headerCss.css" rel="stylesheet" type="text/css"/>
<link href="hardCode/css/searchCss.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <%Integer pageindex = (Integer) request.getAttribute("pageindex");
        Integer pagecount = (Integer) request.getAttribute("pagecount");
        String searchKey =(String) request.getAttribute("searchKey");
        Integer pagegap = (Integer) request.getAttribute("pagegap");
    
        %>
       
    </head>
    <body>
        <%@include file="components/header.jsp"%>
        <div class="container">
            <div class="main-content">
              <c:if test="${not empty requestScope.sqlErr}">
                        <script>
                            alert("${requestScope.sqlErr}");
                        </script>    
                    </c:if>
                <div class="content-left">
                   
                 <!-- search      -->
                 
                   <!--- check whether the the article list is empty or not -->
                 <c:if test="${not empty requestScope.artListerror}">
                     <h3>${requestScope.artListerror} </h3>
                 </c:if>
                <!--- check if the the article list is not empty show the article -->

                 <c:if test="${empty requestScope.artListerror}"> 
                       <!--- Check to see if the page index has errors -->
                         <c:if test="${not empty requestScope.pageIndexErr }">
                             <h3> ${requestScope.pageIndexErr}</h3>
                         </c:if> 
                     <c:if test="${empty requestScope.pageIndexErr }">
                         <c:if test="${not empty requestScope.articleListErr}">
                             <h3>${requestScope.articleListErr}</h3>
                         </c:if>
                         <c:if test="${empty requestScope.articleListErr}">
                 <div class="content-left-link">
                     
                     <!--- the loop to show list of article that have been found by search key -->

               <c:forEach items="${requestScope.articleList}" var="article">
               
                        <h4><a class="text-titlte" href="viewDetail?ID=${article.getId()}"> 
                             ${article.getTitle()}
                        </a>
                        </h4>
                   <div class="block-search">
                       
                        <div class="block-search-img">
                            <a href="viewDetail?ID=${article.getId()}">  <img src="${article.getImage()}"> </a>
                        </div>
                        <div class="block-search-description">
                            <div class="text-top-1">
                           ${article.getDescription()}
                           </div>
                        </div>    
                     </div>
                          </c:forEach>
                     <div class="container-bot2">    
                     <!-- doi lai ten bien-->
                 <%=HtmlHelper.pager(pageindex ,pagecount,pagegap,searchKey)%>
                 </div>
                        </div>
                 
                 </c:if>   
                 </c:if>
                 </c:if>
                     
                  <!-- search      -->
                </div>
                <%@include file="components/rightContent.jsp" %>
                 <div class="footer">
        </div> 
        </div>
            
        </div>
      
    </body>
</html>
