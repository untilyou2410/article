<%-- 
    Document   : home
    Created on : Feb 19, 2020, 1:09:45 PM
    Author     : anhnb
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="hardCode/css/rightCss.css" rel="stylesheet" type="text/css"/>
<link href="hardCode/css/cssHome.css" rel="stylesheet" type="text/css"/>
<link href="hardCode/css/headerCss.css" rel="stylesheet" type="text/css"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="components/header.jsp" %>
        <div class="container">
            <div class="main-content">
               
                <!--      -->
                <div class="content-left">
                    <c:if test="${not empty requestScope.sqlErr}">
                        <script>
                            alert("${requestScope.sqlErr}");
                        </script>    
                    </c:if>
                    <!--- check whether the id error exists or not -->
                    <c:if test="${ not empty requestScope.idErr}">
                        <h3> ${requestScope.idErr}</h3>
                    </c:if>
                        
                    <c:if test="${ empty requestScope.idErr}">
                        <!--- check whether the article error exists or not -->
                        <c:if test="${ not empty requestScope.error}">     
                            <h3> ${requestScope.error}</h3>
                        </c:if>
                           <!--- check whether errors do not exist, show article -->
                        <c:if test="${ empty requestScope.error}">     
                           <div class="container-top">
                                <h3 class="text-titlte">${art.getTitle()}</h3>
                                <img src="${art.getImage()}" alt=""/>
                                <p class="text-top-1"> ${art.getContent()}   </p>
                                  <div class="container-bot">
                                      <div class="text-container-bottom"> <span class="text-genaral1" >By ${art.getAuthor()} | ${art.getDateFormat()} - ${art.getTimeFormat()}</span></div>
                                    <div class="icon1">    
                                    </div>
                                    <div class="icon2"></div>
                                </div>
                            </div>
                            
                        </c:if>
                    </c:if>
                </div>
                
                <%@include file="components/rightContent.jsp" %>
                
            </div>
        </div>
        <div class=" footer">
                </div>   
    </body>
</html>
