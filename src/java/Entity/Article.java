package Entity;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;

/**
 *
 * @author Thaycacac
 */
public class Article {

    private int id;
    private String title;
    private String image;
    private String content;
    private Date date;
    private Time time;
    private String author;
    private String description;

    public Article() {
         
    }

    public Article(int id, String title, String image, String content, Date date, String author) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.content = content;
        this.date = date;
        this.author = author;
        if (content.length() <= 200) {
            this.description = content + "...";
        } else {
            this.description = content.substring(0, 200) + "...";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage() {
        this.image = "ArtiImg/"+this.id+".jpg";
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription() {
         if (content.length() <= 200) {
            this.description = content + "...";
        } else {
            this.description = content.substring(0, 200) + "...";
        }
    }
    public void setTime(Time time){
        this.time = time;
    }
    public Time getTime(){
        return this.time;
    }
    public String getDateFormat() {
        return new SimpleDateFormat("MMM dd yyy").format(this.date);
    }
    
    public String getTimeFormat(){
        return new SimpleDateFormat("H:mmaa").format(this.time).toLowerCase();
    }
    @Override
    public String toString() {
        return "Article{" + "id=" + id + ", title=" + title + ", image=" + image + ", content=" + content + ", date=" + date + ", author=" + author + ", description=" + description + '}';
    }

}
