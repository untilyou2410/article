/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Entity.Article;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anhnb
 */
public class articleDAO extends DBContext {

    public ArrayList<Article> getArtiListPaging(int pageindex, int pagesize, String search) throws Exception {
        Article arti;
        ArrayList<Article> artList = new ArrayList<Article>();
        //String[] searchList = search.trim().split(" ");

        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection connection = this.getConnection();

        try {
            String sql = "select * from \n"
                    + " (select *,ROW_NUMBER() OVER(Order By ID ASC) "
                    + "as row_num from article where content like ? ) tblarticle\n"
                    + " WHERE row_num >= (? - 1)*? +1 AND row_num<= (? * ?) ";
            String sql2 = " AND content  LIKE ?";
            if (!search.equalsIgnoreCase(" ")) {
                sql = sql + sql2;
            }

            statement = connection.prepareStatement(sql);
            statement.setString(1, "%" + search + "%");
            statement.setInt(2, pageindex);
            statement.setInt(3, pagesize);
            statement.setInt(4, pageindex);
            statement.setInt(5, pagesize);
            if (!search.equalsIgnoreCase(" ")) {
                statement.setString(6, "%" + search + "%");
            }

            rs = statement.executeQuery();
            while (rs.next()) {
                arti = new Article();
                arti.setId(rs.getInt("ID"));
                arti.setTitle(rs.getString("tittle"));
                arti.setContent(rs.getString("content"));
                arti.setDate(rs.getDate("datePub"));
                arti.setTime(rs.getTime("datePub"));
                arti.setAuthor(rs.getString("author"));
                arti.setDescription();
                arti.setImage();
                artList.add(arti);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.closeConnection(rs, statement, connection);
        }

        return artList;
    }

    public int artCount(String search) throws Exception {
        int count = 0;
        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection connection = this.getConnection();

        try {
            String sql = "select COUNT (*) as rownum from article ";
            String sql1 = "where content LIKE ? ";
            if (!search.equalsIgnoreCase(" ")) {
                sql += sql1;
            }
            statement = connection.prepareStatement(sql);
            if (!search.equalsIgnoreCase(" ")) {
                statement.setString(1, "%" + search + "%");
            }
            rs = statement.executeQuery();
            while (rs.next()) {
                count = rs.getInt("rownum");
            }

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.closeConnection(rs, statement, connection);
        }

        return count;
    }

    public ArrayList<Article> getArtiListTop5() throws Exception {
        Article arti;
        ArrayList<Article> artList = new ArrayList<Article>();

        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection connection = this.getConnection();
        try {
            String sql = "SELECT TOP 5 [ID]\n"
                    + "      ,[tittle]\n"
                    + "      ,[image]\n"
                    + "      ,[content]\n"
                    + "      ,[datePub]\n"
                    + "      ,[author]\n"
                    + "  FROM [article] order by ID desc";
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                arti = new Article();
                arti.setId(rs.getInt("ID"));
                arti.setTitle(rs.getString("tittle"));
                arti.setImage();
                arti.setContent(rs.getString("content"));
                arti.setDate(rs.getDate("datePub"));
                arti.setTime(rs.getTime("datePub"));
                arti.setAuthor(rs.getString("author"));
                arti.setDescription();
                artList.add(arti);
            }

        } catch (Exception ex) {
             throw ex;
        } finally {
            this.closeConnection(rs, statement, connection);
        }
        return artList;
    }

    public Article getArticleByID(int ID) throws Exception {
        Article arti = null;

        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection connection = this.getConnection();
        try {
            String sql = "SELECT  [ID]\n"
                    + "      ,[tittle]\n"
                    + "      ,[image]\n"
                    + "      ,[content]\n"
                    + "      ,[datePub]\n"
                    + "      ,[author]\n"
                    + "  FROM article where ID = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, ID);
            rs = statement.executeQuery();

            while (rs.next()) {
                arti = new Article();
                arti.setId(rs.getInt("ID"));
                arti.setTitle(rs.getString("tittle"));
                arti.setImage();
                arti.setContent(rs.getString("content"));
                arti.setDate(rs.getDate("datePub"));
                arti.setTime(rs.getTime("datePub"));
                arti.setAuthor(rs.getString("author"));
                arti.setDescription();
                return arti;
            }

        } catch (Exception ex) {
             throw ex;
        } finally {
            this.closeConnection(rs, statement, connection);
        }

        return null;

    }

}
