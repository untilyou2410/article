/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.articleDAO;
import Entity.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anhnb
 */
public class viewDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {

            //check id is number or not
            int ID = 0;
            String raw_ID = request.getParameter("ID");;
            if(raw_ID==null){
                request.setAttribute("error","NOT FOUND ID");
            }
            else{
                if (raw_ID.matches("[0-9]+") == false) {
                request.setAttribute("idErr", "Enter ID again");
            } else {
                ID = Integer.parseInt(raw_ID);
            }
            }
            
            articleDAO arDAO = new articleDAO();
            //get top 5 recent article find it null or not 
            ArrayList<Article> top5Art = new ArrayList<>();
            try {
                top5Art = arDAO.getArtiListTop5();
            } catch (Exception e) {
                request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }

            if (top5Art.isEmpty()) {
                request.setAttribute("top5ArtErr", "NOT FOUND TOP 5 RECENT!!");
            } else {
                request.setAttribute("top5Art", top5Art);
            }

            //get the article that has id== id parameter
            //check article is null or not
            Article art = null;
            try {
                art = arDAO.getArticleByID(ID);
                 if (art == null) {
                request.setAttribute("error", "NOT FOUND ID");
            }
            } catch (Exception e) {
                request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }
            request.setAttribute("art", art);
           
            request.getRequestDispatcher("viewDetail.jsp").forward(request, response);
        } catch (Exception ex) {
            request.getRequestDispatcher("errorPage.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
