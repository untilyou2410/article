/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.articleDAO;
import Entity.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anhnb
 */
public class home extends HttpServlet {

    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            articleDAO artDAO = new articleDAO();

            //get Article and check they null or not 
            ArrayList<Article> top5Art = new ArrayList<>();
            try {
                top5Art = artDAO.getArtiListTop5();
            } catch (Exception e) {
                request.setAttribute("sqlErrTop5", "CAN NOT LOAD DATA!!!");
            }
            
            if (top5Art.isEmpty()) {
                request.setAttribute("top5ArtErr", "NOT FOUND TOP 5 RECENT!!");
            } else {
                request.setAttribute("top5Art", top5Art);
            }

            //get home's article and check it null or not
            Article art = null ;
            try {
                art=artDAO.getArticleByID(1);
                if (art == null) {
                request.setAttribute("error", "NOT FOUND HOME ARTICLE");
            }
            } catch (Exception e) {
                 request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }
            request.setAttribute("art", art);
            

            request.getRequestDispatcher("home.jsp").forward(request, response);
        } catch (Exception ex) {

            request.getRequestDispatcher("errorPage.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
