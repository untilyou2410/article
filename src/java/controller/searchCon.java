/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.articleDAO;
import Entity.Article;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anhnb
 */
public class searchCon extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Article art = new Article();
            articleDAO artDAO = new articleDAO();
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset = UTF-8");

            //there are 3 articles that have been showed in one page
            int pagesize = 3;
            int pagegap = 2;
            //get parameter to search and check it null or not
            String searchKey = "";
            String raw_search = request.getParameter("search");
            if (raw_search == null || raw_search.trim().length()==0) {
                request.setAttribute("searchErr", "Enter something");
            } else {
                searchKey = raw_search;
            }

            //retrieves the index of the page the user is in
            //and check it null, corret form or not  
            int pageindex = 0;
            String raw_pageindex = request.getParameter("page");
            if (raw_pageindex == null) {
                raw_pageindex = "1";
            }
            if (raw_pageindex.matches("[0-9]+") == false) {
                request.setAttribute("pageIndexErr", "Enter page index again index ");
            } else {
                pageindex = Integer.parseInt(raw_pageindex);
            }

            ArrayList<Article> artiList = null;
            //get article that in same page index
            try {
                artiList = artDAO.getArtiListPaging(pageindex, pagesize, searchKey);
            } catch (Exception e) {
                request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }
             //check size of article 
            if (artiList.size() == 0) {
                request.setAttribute("artListerror", "NOT FOUND ANY ARTICLE");
            } else {
                request.setAttribute("articleList", artiList);
            }
            //count number of article that have same search key
            int count =0;
            try {
                count = artDAO.artCount(searchKey);
            } catch (Exception e) {
                request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }
            //caculated the number of page to pagging
            int pagecount = (count % pagesize == 0) ? count / pagesize : count / pagesize + 1;

            //get top 5 recent article find it null or not 
             ArrayList<Article> top5Art = new ArrayList<>();
            try {
                top5Art = artDAO.getArtiListTop5();
            } catch (Exception e) {
                request.setAttribute("sqlErr", "CAN NOT LOAD DATA!!!");
            }
            
            if (top5Art.isEmpty()) {
                request.setAttribute("top5ArtErr", "NOT FOUND TOP 5 RECENT!!");
            } else {
                request.setAttribute("top5Art", top5Art);
            }

            request.setAttribute("pagegap", pagegap);
            request.setAttribute("pagecount", pagecount);
            request.setAttribute("pageindex", pageindex);
            request.setAttribute("searchKey", searchKey);

           

            request.getRequestDispatcher("searchArt.jsp").forward(request, response);
        } catch (Exception ex) {
            request.setAttribute("error", "ERROR!!!!");
            request.getRequestDispatcher("errorPage.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
