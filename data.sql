USE [labWeb]
GO
/****** Object:  Table [dbo].[article]    Script Date: 9/23/2020 11:04:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[article](
	[ID] [int] NOT NULL,
	[tittle] [nvarchar](max) NULL,
	[image] [nvarchar](50) NULL,
	[content] [nvarchar](max) NULL,
	[datePub] [datetime2](7) NULL,
	[author] [nvarchar](50) NULL,
	[time] [time](7) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (1, N'Making security feeds smater via machine learning', NULL, N'In today age of surveilance, with cameras even in our homes, security professionals need to monitor an ever-increasing number of screens. In 2014 alone,245 milion video surveillance cameras were installed globally, according to an HIS report. It is increasingly clear that the watchmen are having trouble watching. This is where Singapore-bassed Vi Dimensions hopes to  help by identifying anomalies to alert human operators that a second look is needed<br> <br>
In today age of surveilance, with cameras even in our homes, security professionals need to monitor an ever-increasing number of screens. In 2014 alone,245 milion video surveillance cameras were installed globally, ng trouble watching. This is where Singapore-bassed Vi Dimensions hopes to  help by identifying anomalies to alert human operators that a second look is needed
 
', CAST(0x0700D4F3B7255B950A AS DateTime2), N'Benjamin Cher', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (2, N'Nữ DJ nóng bỏng vừa vào nghề đã bị khách chuốc thuốc trong quán bar là ai?', NULL, N'DJ Sunny (Đặng Diễm My, sinh năm 1995, quê Kiên Giang), là gương mặt DJ mới nổi tại TP.HCM. Cô từng lọt vào top 5 cuộc thi Miss DJ 2015 (cuộc thi tìm kiếm các tài năng DJ trẻ), tham gia nhiều sự kiện âm nhạc ngoài trời cùng các tên tuổi lớn trong Vpop. Người đẹp 9X vừa gây chú ý khi tuyên bố mình là "DJ hiền nhất Đông Nam Bộ". 

Giải thích về danh xưng này, Sunny bộc bạch với chúng tôi rằng: "Tuy tôi là DJ, đam mê nhạc điện tử và công việc đa số làm về đêm, nhưng tôi không có thói quen vui chơi thâu đêm. Tôi càng nói không với những chất kích thích như những lời đồn đại rằng “DJ phải có chất kích thích mới chơi nhạc hay được". Sau giờ làm, tôi chỉ muốn chạy thẳng về nhà, tiếp tục sáng tạo những điều mới lạ trong tiếc mục biểu diễn. Tôi muốn khán giả thấy được Sunny còn nhiều điều thú vị trong những set nhạc của mình".

Hiện ngoài hoạt động DJ, Sunny còn phát triển kênh YouTube riêng về lĩnh vực xã hội, làm đẹp... thu hút lượt xem lớn từ khán giả.', CAST(0x0700000000005B950A AS DateTime2), N'aaaa', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (3, N'aaaa', NULL, N'Nhân dịp năm mới, năm Canh Tý 2020. Chuyên khoa Tiểu Đường của Bệnh Viện Quân Đội gồm các cán bộ, bác sĩ, y sĩ, dược sĩ trực tiếp thăm khám và phát thuốc Miễn phí cho bệnh nhân bẹ bệnh tiểu đường, trên địa bàn Hà Nội và gửi thuốc Miễn phí cho các Bệnh nhân ở tỉnh thành khác trong Toàn quốc.
Các Đại biểu tham dự chương trình
Đây là một hoạt động thường niên của Bệnh viện Quân Đội phối hợp với các nhà hảo tâm, nhằm giúp đỡ phần nào cho bệnh nhân bị tiểu đường trên toàn quốc nhân dịp đầu năm mới, năm Canh Tý 2020.
Tham dự chương trình có Đại tá Đặng Duy Quý, Trưởng ban Giáo vụ; TS. Nguyễn Thị Phi Nga Giam Đốc Bệnh viện Quân Đội; Tổng biên tập báo Quân đội nhân dân; cùng 60 y, bác sĩ, dược sĩ của Bệnh viện.', CAST(0x0700000000005B950A AS DateTime2), N'Van ngao', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (4, N'Chủ tịch Hà Nội: Chiều 28/2 sẽ quyết định học sinh đi học hay nghỉ tiếp', NULL, N'Sáng 26/2, trao đổi với phóng viên về việc liệu Thành phố có quyết định cho học sinh đi học vào ngày 2/3, như thông tin được đưa ra tại cuộc họp Ban chỉ đạo phòng chống dịch Covid-19 vào ngày 21/2 hay không, ông Nguyễn Đức Chung - Chủ tịch UBND TP Hà Nội cho biết, đến chiều thứ 6 ngày 28/2, Thành phố sẽ có quyết định cụ thể.

Theo Chủ tịch UBND TP Hà Nội Nguyễn Đức Chung, việc cho học sinh đi học trở lại như thế nào phải căn cứ vào tình hình dịch Covid-19 có an toàn hay không.

“Phải khẳng định đến giờ phút này Thành phố chưa có trường hợp nào phát hiện lây chéo. Bên cạnh đó, những trường hợp đi từ vùng dịch về được kiểm soát chặt chẽ và đều chủ động cách ly”, ông Chung nói.', CAST(0x0700000000005B950A AS DateTime2), N'haha', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (9, N'Hà Nội: Học sinh nghỉ đến 8/3, riêng trường quốc tế đi học từ 2/3', NULL, N'Trước băn khoăn của vị bác sĩ trẻ, ông Vũ Mạnh Cường đã đưa ra lời khuyên: “nếu cậu muốn thì nên đề nghị được cách ly tập trung ngay sau khi nhập cảnh ở Nội Bài. Hà cớ gì đi xe về tỉnh để phải băn khoăn về những người đi cùng xe. Sau khi nhập cảnh, những người thuộc diện cách ly sẽ được Kiểm dịch Quốc tế sân bay Nội Bài và y tế Hà Nội hướng dẫn. Hà Nội đang có 2 địa chỉ cách ly tập trung là Bệnh viện Công an và Trường Quân sự ở Sơn Tây”.
Cuối cùng vị bác sĩ trẻ quyết định khi anh từ Nhật Bản về vào đầu tháng tới, sẽ chủ động tham gia cách ly luôn.
Có thể nói quyết định của vị bác sĩ là một hành xử rất văn minh, hiểu rõ vấn đề, nguy cơ và có trách nhiệm đối với sức khỏe của bản thân, gia đình và cộng đồng.
Với tình hình dịch bệnh viêm đường hô hấp cấp do chủng mới virus Corona (COVID - 19) gây ra với tốc độ lây lan nhanh chóng như hiện nay, việc cách ly nhằm ngăn ngừa phơi nhiễm hoặc nghi ngờ mắc bệnh là việc làm vô cùng cấp thiết.
Hiện tại những người nhập cảnh từ Hàn Quốc và các quốc gia, vùng lãnh thổ công bố có dịch, sẽ được áp dụng biện pháp như với Trung Quốc, hạn chế và khuyến cáo không vào Việt Nam; thực hiện khai báo y tế khi vào Việt Nam, và thực hiện cách ly tập trung theo qui định. Đồng thời, cũng khuyến cáo công dân Việt Nam không đến vùng có dịch và nếu vì lý do nhất thiết phải đi thì khi trở về, nhập cảnh vào Việt Nam phải thực hiện cách ly tập trung.', CAST(0x0700000000005B950A AS DateTime2), N'Van ngaoVan ngao', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (5, N'Đăng tin sai sự thật “Tuấn “khỉ” gia nhập tình báo”, một phụ nữ bị phạt', NULL, N'Theo đó, vào ngày 18/2, Sở Thông tin và Truyền thông (TT&TT) tỉnh Cà Mau phát hiện một tài khoản Facebook có chia sẻ đoạn video với nội dung: “Hồ sơ mật: Tuấn Khỉ gia nhập tình báo Hoa Nam, Bộ CA bất lực dựng hiện trường giả...”.

Qua xác minh, cơ quan chức năng xác định chủ tài khoản đăng thông tin nói trên là một người phụ nữ tên T. ở huyện Trần Văn Thời (tỉnh Cà Mau).

Ngày 20/2, Sở TT&TT phối hợp cùng Công an tỉnh Cà Mau và đơn vị chức năng tiến hành mời người đăng tin lên làm việc. Bà T. khai nhận, do thấy trên mạng xã hội có đăng video với nội dung trên nên đã chia sẻ cho mọi người xem chứ không xem nội dung trong video nói những gì.

Đồng thời, bà T. cũng cho rằng, do không biết chia sẻ đoạn video chứa thông tin chưa được kiểm chứng, có nội dung xuyên tạc, xúc phạm uy tín của tổ chức, danh dự nhân phẩm của người khác trên mạng xã hội là vi phạm pháp luật nên mới chia sẻ. Sau đó, bà T. đã xóa bỏ đoạn video.', CAST(0x0700000000005B950A AS DateTime2), N'dddd', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (6, N'Kỳ lạ cầu 30 tỷ hoàn thành 6 năm vẫn chưa... thông xe', NULL, N'Kỳ lạ cầu 30 tỷ hoàn thành 6 năm vẫn chưa... thông xeKỳ lạ cầu 30 tỷ hoàn thành 6 năm vẫn chưa... thông xeKỳ lạ cầu 30 tỷ hoàn thành 6 năm vẫn chưa... thông xeKỳ lạ cầu 30 tỷ hoàn thành 6 năm vẫn chưa... thông xe', CAST(0x0700000000005B950A AS DateTime2), N'ddddd', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (7, N'Hà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn Quốc', NULL, N'Hà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn QuốcHà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn Quốc', CAST(0x0700000000005B950A AS DateTime2), N'dddd', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (8, N'COVID-19: Bác sĩ Việt từ Nhật trở về xin tự nguyện cách ly dù... vẫn khoẻ', NULL, N'Theo tình hình hiện nay vị bác sĩ này chưa phải đối tượng cần cách ly bắt buộc, tuy nhiên anh vẫn muốn chủ động cách ly 14 ngày để đảm bảo an toàn cho gia đình và những người xung quanh. Anh muốn tìm hiểu trong trường hợp của anh thì nên cách ly theo hình thức nào là tốt nhất, “tham gia cách ly luôn sau khi nhập cảnh vào Nội Bài hay trở về tỉnh nhà để tự cách ly?”
Trước băn khoăn của vị bác sĩ trẻ, ông Vũ Mạnh Cường đã đưa ra lời khuyên: “nếu cậu muốn thì nên đề nghị được cách ly tập trung ngay sau khi nhập cảnh ở Nội Bài. Hà cớ gì đi xe về tỉnh để phải băn khoăn về những người đi cùng xe. Sau khi nhập cảnh, những người thuộc diện cách ly sẽ được Kiểm dịch Quốc tế sân bay Nội Bài và y tế Hà Nội hướng dẫn. Hà Nội đang có 2 địa chỉ cách ly tập trung là Bệnh viện Công an và Trường Quân sự ở Sơn Tây”.
Cuối cùng vị bác sĩ trẻ quyết định khi anh từ Nhật Bản về vào đầu tháng tới, sẽ chủ động tham gia cách ly luôn.
Có thể nói quyết định của vị bác sĩ là một hành xử rất văn minh, hiểu rõ vấn đề, nguy cơ và có trách nhiệm đối với sức khỏe của bản thân, gia đình và cộng đồng.
Với tình hình dịch bệnh viêm đường hô hấp cấp do chủng mới virus Corona (COVID - 19) gây ra với tốc độ lây lan nhanh chóng như hiện nay, việc cách ly nhằm ngăn ngừa phơi nhiễm hoặc nghi ngờ mắc bệnh là việc làm vô cùng cấp thiết.
Hiện tại những người nhập cảnh từ Hàn Quốc và các quốc gia, vùng lãnh thổ công bố có dịch, sẽ được áp dụng biện pháp như với Trung Quốc, hạn chế và khuyến cáo không vào Việt Nam; thực hiện khai báo y tế khi vào Việt', CAST(0x0700000000005B950A AS DateTime2), N'Xuân Diệu', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (10, N'Hà Nội: Học sinh nghỉ đến 8/3, riêng trường quốc tế đi học từ 2', NULL, N'Trên cơ sở đề xuất của liên ngành, Chủ tịch UBND TP Hà Nội Nguyễn Đức Chung quyết định cho khoảng 2 triệu học sinh từ mầm non cấp 3 tiếp tục nghỉ học đến 8/3.

Với 284 cơ sở dạy nghề (khoảng 180.000 học viên), Chủ tịch UBND TP Hà Nội chốt đi học từ ngày 2/3, như đề xuất của liên ngành. “Đối với các trường quốc tế, do liên quan đến lịch học của nước ngoài, chúng ta cũng đồng ý theo đề nghị của các trường, đi học trở lại trừ ngày 2/3”, ông Chung nói.

Kết lại buổi họp, ông Nguyễn Đức Chung - Chủ tịch UBND TP Hà Nội một lần nữa yêu cầu các trường học đảm bảo đầy đủ cơ sở để đón học sinh tới trường trong thời gian tới. “Với học sinh THPT sẽ quay lại trường từ ngày 9/3; còn học sinh từ mầm non đến THCS, chúng tôi sẽ quyết lịch học vào cuối tuần tới”, ông Chung nói thêm.

Tại cuộc họp, ông Chử Xuân Dũng - Giám đốc Sở Giáo dục và Đào tạo Hà Nội cho biết, các trường học trên toàn địa bàn Thành phố vẫn đang chuẩn bị các điều kiện cần thiết để chuẩn bị đón học sinh quay lại trường. “Tuy nhiên, nguy cơ lây nhiễm tại các trường học vẫn còn, tâm lý phụ huynh cũng vẫn chưa yên tâm khi đưa con đến trường”, ông Dũng nói.', CAST(0x0700000000005B950A AS DateTime2), N'Anh Thư', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (11, N'Hà Nội: Học sinh nghỉ đến 8/3, riêng trường quốc tế đi học từ 2', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x0700D66F4EB65B950A AS DateTime2), N'Naruto', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (12, N'Hà Nội họp khẩn vì dịch Covid-19 lan rộng ở Hàn Quốc', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x070058A5C8C055450B AS DateTime2), N'Sasuke', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (13, N'Cuộc sống trong con phố cách ly của người Hà Nội', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x0700021F78A255450B AS DateTime2), N'Obito', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (14, N'Cách người Mỹ tự cách ly ở nhà', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x0700021F78A255450B AS DateTime2), N'Gon', NULL)
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (15, N'hieu aaa dd aa', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x0700021F78A255450B AS DateTime2), N'Đen Vâu', CAST(0x07004C64EB810000 AS Time))
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (16, N'Chủ tịch Hà Nội: Chiều 28/2 sẽ quyết định học sinh đi học hay nghỉ tiếp', NULL, N'Giám đốc Sở Giáo dục và Đào tạo Hà Nội cũng thông tin, các trường quốc tế trên địa bàn TP Hà Nội có đề xuất cho học sinh đi học trở lại từ ngày 2/3 tới. Các trường này đưa ra lý do nhà trường đã chủ động mới đại diện Tổ chức Y tế thế giới đến trao đổi, học sinh trong trường không đông, các trường cũng đảm bảo công tác khử trùng…

Cho ý kiến tại cuộc họp, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội cho biết, đa số phụ huy học sinh tỏ ra lo ngại tình huống học sinh quay lại trường khi dịch bệnh đang tăng nhanh ở một số nước. Đặc biệt phụ huynh học sinh từ cấp 2 trở xuống càng lo lắng hơn.

“Thành phố cần tính toán kỹ cho học sinh cấp nào quay trở lại trường học trong thời gian tới. Tuy nhiên, tâm lý chung, mọi người đều mong muốn cho học sinh nghỉ thêm một tuần nữa, vì lúc đó người từ vùng dịch trở về đủ thời gian cách ly 14 ngày”, đại diện Ủy ban MTTQ Việt Nam TP Hà Nội nói.

Chủ tịch UBND quận Hà Đông Vũ Ngọc Phụng cho biết, khó khăn lớn nhất của quận Hà Đông nếu cho học sinh đi học trở lại là thiếu thiết bị đo thân nhiệt. Do vậy, quận Hà Đông kiến nghị TP giới thiệu cơ sở mua thiết bị đo thân nhiệt đảm bảo chất lượng và số lượng.

Ông Nguyễn Xuân Lưu - Bí thư, Chủ tịch UBND quận Thanh Xuân cho hay, đến nay các trường trên địa bàn quận chuẩn bị mọi điều kiện để sẵn sàng đón học sinh tới trường. “Tuy nhiên, dư luận nhân dân, phụ huynh đến thời điểm này rất mong muốn cho học sinh nghỉ tiếp”, ông Lưu cho biết.

Báo cáo tại cuộc họp, đại diện Sở Y tế Hà Nội cho biết, tính đến 15h, chiều 28/2, Hà Nội chưa ghi nhận trường hợp nào dương tính với Covid-19; có 85 trường hợp nghi nhiễm được giám sát tại bệnh', CAST(0x0700021F78A25B950A AS DateTime2), N'Mít', CAST(0x07004E990C2A0000 AS Time))
INSERT [dbo].[article] ([ID], [tittle], [image], [content], [datePub], [author], [time]) VALUES (17, N'ngay hom qua do ta nhin thay nhau', NULL, N'Theo tình hình hiện nay vị bác sĩ này chưa phải đối tượng cần cách ly bắt buộc, tuy nhiên anh vẫn muốn chủ động cách ly 14 ngày để đảm bảo an toàn cho gia đình và những người xung quanh. Anh muốn tìm hiểu trong trường hợp của anh thì nên cách ly theo hình thức nào là tốt nhất, “tham gia cách ly luôn sau khi nhập cảnh vào Nội Bài hay trở về tỉnh nhà để tự cách ly?”
Trước băn khoăn của vị bác sĩ trẻ, ông Vũ Mạnh Cường đã đưa ra lời khuyên: “nếu cậu muốn thì nên đề nghị được cách ly tập trung ngay sau khi nhập cảnh ở Nội Bài. Hà cớ gì đi xe về tỉnh để phải băn khoăn về những người đi cùng xe. Sau khi nhập cảnh, những người thuộc diện cách ly sẽ được Kiểm dịch Quốc tế sân bay Nội Bài và y tế Hà Nội hướng dẫn. Hà Nội đang có 2 địa chỉ cách ly tập trung là Bệnh viện Công an và Trường Quân sự ở Sơn Tây”.<br> <br>
Cuối cùng vị bác sĩ trẻ quyết định khi anh từ Nhật Bản về vào đầu tháng tới, sẽ chủ động tham gia cách ly luôn.
Có thể nói quyết định của vị bác sĩ là một hành xử rất văn minh, hiểu rõ vấn đề, nguy cơ và có trách nhiệm đối với sức khỏe của bản thân, gia đình và cộng đồng.
Với tình hình dịch bệnh viêm đường hô hấp cấp do chủng mới virus Corona (COVID - 19) gây ra với tốc độ lây lan nhanh chóng như hiện nay, việc cách ly nhằm ngăn ngừa phơi nhiễm hoặc nghi ngờ mắc bệnh là việc làm vô cùng cấp thiết.
Hiện tại những người nhập cảnh từ Hàn Quốc và các quốc gia, vùng lãnh thổ công bố có dịch, sẽ được áp dụng biện pháp như với Trung Quốc, hạn chế và khuyến cáo không vào<br> Việt Nam; thực hiện khai báo y tế khi vào Việt
', CAST(0x0700CF1B3DA0233F0B AS DateTime2), N'Đuông', CAST(0x0700021F78A20000 AS Time))
