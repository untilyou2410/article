<%-- 
    Document   : rightContent
    Created on : Mar 5, 2020, 10:30:00 PM
    Author     : anhnb
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="../hardCode/css/rightCss.css" rel="stylesheet" type="text/css"/>
        <link href="../hardCode/css/cssHome.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="content-right">
            <div class="container-right">
                <div class="description">
                    <h4 class="text-titlte">
                        Digital News
                    </h4>
                    <p class="text-top-1">
                        ${requestScope.top5Art.get(0).getDescription()}
                    </p>    

                </div>
                <div class="input-se">
                    <h4 class="text-titlte">
                        Search
                    </h4>
                    <c:if test="${not empty requestScope.searchErr}"> <span>${requestScope.searchErr}</span> </c:if>
                    <form method="get" action="searchCon">
                        <input type="text" name="search" required></input>
                        <button> Go</button>
                    </form>
                </div>   
                <div>
                    <h4 class="text-titlte">
                        Last Articles
                    </h4>
                <c:if test="${not empty requestScope.top5ArtErr}">
                    <h3>${requestScope.top5ArtErr}</h3>
                </c:if>
                <c:if test="${empty requestScope.top5ArtErr}">
                    <c:forEach items="${requestScope.top5Art}" var="article">
                        <p>
                            <a href="viewDetail?ID=${article.getId()}" class="text-genaral1">
                            ${article.getTitle()}
                            </a>
                        </p>
                    </c:forEach>
                        </c:if>
                </div>    

            </div>   

        </div>
    </div>
</body>
</html>
